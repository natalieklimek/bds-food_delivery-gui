# BDS - Project

Food delivery project. Simple GUI in JavaFX with DB.

## To build&run the project
Enter the following command in the project root directory to build the project.
```shell
$ ./mvnw clean install
```

Run the project (wait for a second to start-up the service):
```shell
$ java -jar target/bds-rest-java-1.0.0.jar
```


Sign-in with the following credentials:
- Username: `test`
- Password: `heslo`

## To generate the project and external libraries licenses
Enter the following command in the project root directory
```shell
$ ./mvnw project-info-reports:dependencies
```
