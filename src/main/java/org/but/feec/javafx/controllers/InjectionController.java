package org.but.feec.javafx.controllers;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.but.feec.javafx.api.PersonBasicView;
import org.but.feec.javafx.data.InjectionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;


public class InjectionController {

    public static class OutputWrapper {
        private String val;

        public OutputWrapper(String val) {
            this.val = val;
        }

        public String getVal() {
            return this.val;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @FXML
    private TableColumn<OutputWrapper, String> outColumn;
    @FXML
    private TableView<OutputWrapper> displayTable;
    @FXML
    private Button execButton;
    @FXML
    private TextField injectionInput;

    private InjectionRepository injectionRepository;

    @FXML
    private void initialize() {
        initServices();

        this.injectionInput.setText("' UNION SELECT 'test' --");
        outColumn.setCellValueFactory(new PropertyValueFactory<>("val"));

        this.execButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String input = injectionInput.getText();
                List<OutputWrapper> queryOutput = injectionRepository.executeQuery(input)
                        .stream()
                        .map(OutputWrapper::new)
                        .collect(Collectors.toList());
                ObservableList<OutputWrapper> observableList = FXCollections.observableList(queryOutput);

                displayTable.setItems(observableList);
                displayTable.refresh();
                displayTable.sort();
            }
        });
    }

    private void initServices() {
        this.injectionRepository = new InjectionRepository();
    }
}
