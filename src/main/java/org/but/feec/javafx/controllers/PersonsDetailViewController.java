package org.but.feec.javafx.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.api.PersonContactView;
import org.but.feec.javafx.api.PersonDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonsDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsDetailViewController.class);

    @FXML
    private TextField idTextField;
    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TableColumn<PersonContactView, String> contactType;
    @FXML
    private TableColumn<PersonContactView, String> contactValue;
    @FXML
    private TableView<PersonContactView> contactsPane;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        firstNameTextField.setEditable(false);
        lastNameTextField.setEditable(false);

        loadPersonsData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView) {
            PersonDetailView personBasicView = (PersonDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(personBasicView.getId()));
            firstNameTextField.setText(personBasicView.getFirstName());
            lastNameTextField.setText(personBasicView.getLastName());

            contactType.setCellValueFactory(new PropertyValueFactory<PersonContactView, String>("type"));
            contactValue.setCellValueFactory(new PropertyValueFactory<PersonContactView, String>("value"));

            contactsPane.setItems(personBasicView.getContactViews());
        }
    }

}
