package org.but.feec.javafx.data;

import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InjectionRepository {
    public List<String> executeQuery(String param) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT full_name FROM bpc_bds.sql_injection_table1 s" +
                     " WHERE s.email = '" + param + "'")) {

            List<String> result = new ArrayList<>();
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                result.add(rs.getString("full_name"));
            }

            return result;
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }
    }
}
