package org.but.feec.javafx.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.javafx.api.*;
import org.but.feec.javafx.controllers.PersonsDetailViewController;
import org.but.feec.javafx.data.PersonRepository;

import java.util.List;

/**
 * Class representing business logic on top of the Persons
 */
public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public PersonDetailView getPersonDetailView(Long id) {
        PersonBasicView basicView = this.personRepository.getPersonBasicView(id);
        List<PersonContactView> contacts = this.personRepository.getPersonsContacts(id);

        return this.mapToPersonDetail(basicView, contacts);
    }

    public List<PersonBasicView> getPersonsBasicView() {
        return personRepository.getPersonsBasicView();
    }

    public void createPerson(PersonCreateView personCreateView) {
        // the following three lines can be written in one code line (only for more clear explanation it is written in three lines
        char[] originalPassword = personCreateView.getPwd();
        char[] hashedPassword = hashPassword(originalPassword);
        personCreateView.setPwd(hashedPassword);

        personRepository.createPerson(personCreateView);
    }

    public void editPerson(PersonEditView personEditView) {
        personRepository.editPerson(personEditView);
    }

    public void deletePerson(Long id) {
        this.personRepository.deletePerson(id);
    }

    /**
     * <p>
     * Note: For implementation details see: https://github.com/patrickfav/bcrypt
     * </p>
     *
     * @param password to be hashed
     * @return hashed password
     */
    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }

    private PersonDetailView mapToPersonDetail(PersonBasicView basicView, List<PersonContactView> contacts) {
        PersonDetailView detailView = new PersonDetailView();

        detailView.setId(basicView.getId());
        detailView.setFirstName(basicView.getFirstName());
        detailView.setLastName(basicView.getLastName());
        detailView.setContactViews(contacts);

        return detailView;
    }
}